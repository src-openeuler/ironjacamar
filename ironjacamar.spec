%global namedreltag .Final
%global namedversion %{version}%{?namedreltag}
Name:                ironjacamar
Version:             1.3.4
Release:             2
Summary:             Java Connector Architecture 1.7 implementation
License:             LGPLv2+
URL:                 http://www.ironjacamar.org/
Source0:             https://github.com/ironjacamar/ironjacamar/archive/%{name}-%{namedversion}.tar.gz
Patch0:              ironjacamar-1.3.4.Final-Use-local-provided-IVY.patch
Patch1:              0002-Adjust-IVY-paths.patch
Patch2:              ironjacamar-1.3.4-remove-ambiguous-reference-to-tracef.patch
BuildRequires:       ant apache-ivy graphviz ivy-local java-devel javapackages-local
BuildRequires:       mvn(javax.validation:validation-api) mvn(jdepend:jdepend)
BuildRequires:       mvn(org.apache.ant:ant) mvn(org.jboss:jandex)
BuildRequires:       mvn(org.jboss:jboss-transaction-spi) mvn(org.jboss.apiviz:apiviz)
BuildRequires:       mvn(org.jboss.jdeparser:jdeparser) mvn(org.jboss.logging:jboss-logging)
BuildRequires:       mvn(org.jboss.logging:jboss-logging-annotations)
BuildRequires:       mvn(org.jboss.logging:jboss-logging-processor)
BuildRequires:       mvn(org.jboss.logmanager:jboss-logmanager)
BuildRequires:       mvn(org.jboss.logmanager:log4j-jboss-logmanager)
BuildRequires:       mvn(org.jboss.spec.javax.security.auth.message:jboss-jaspi-api_1.0_spec)
BuildRequires:       mvn(org.jboss.spec.javax.transaction:jboss-transaction-api_1.2_spec)
BuildRequires:       mvn(org.jboss.threads:jboss-threads) mvn(org.jgroups:jgroups)
BuildRequires:       mvn(org.picketbox:picketbox)
Requires:            java-headless javapackages-tools mvn(javax.validation:validation-api)
Requires:            mvn(org.jboss:jandex) mvn(org.jboss:jboss-transaction-spi)
Requires:            mvn(org.jboss.logging:jboss-logging)
Requires:            mvn(org.jboss.spec.javax.security.auth.message:jboss-jaspi-api_1.0_spec)
Requires:            mvn(org.jboss.spec.javax.transaction:jboss-transaction-api_1.2_spec)
Requires:            mvn(org.jboss.threads:jboss-threads)
Requires:            mvn(org.jboss.logmanager:log4j-jboss-logmanager) mvn(org.picketbox:picketbox)
BuildArch:           noarch
%description
The IronJacamar project implements the Java Connector Architecture 1.7
specification.
The Java Connector Architecture (JCA) defines a standard architecture for
connecting the Java EE platform to heterogeneous Enterprise Information
Systems (EIS). Examples of EISs include Enterprise Resource Planning (ERP),
mainframe transaction processing (TP), database and messaging systems.

%package help
Summary:             Javadoc for %{name}
provides:            %{name}-help = %{version}-%{release}
obsoletes:           %{name}-help < %{version}-%{release}
%description help
This package contains the API documentation for %{name}.

%prep
%setup -qn %{name}-%{name}-%{namedversion}
find -name "*.class" -print -delete
find -name "*.jar" -print -delete
%patch0 -p1
%patch1 -p1
%patch2 -p1
files='
api/src/main/java/javax/resource/spi/BootstrapContext.java
api/src/main/java/javax/resource/spi/work/SecurityContext.java
'
for f in ${files}; do
  native2ascii -encoding UTF8 ${f} ${f}
done
cp -r doc/licenses/lgpl-2.1.txt LICENSE.txt
mkdir lib
sed -i '/IronJacamar requires JDK7/d' build.xml
sed -i '/Nexus requires JDK7/d' build.xml
sed -i '/<deploy-file/d' build.xml
sed -i '/<install-file/d' build.xml

%build
ant -Divy.mode=local -Das jars-base clean docs nexus-base
%pom_change_dep org.jboss.spec.javax.transaction: :jboss-transaction-api_1.2_spec target/%{name}-core-api.xml
%pom_change_dep javax.validation: :validation-api target/%{name}-core-impl.xml
%pom_change_dep org.jboss.spec.javax.security.auth.message: :jboss-jaspi-api_1.0_spec target/%{name}-core-impl.xml
%pom_change_dep org.jboss.spec.javax.transaction: :jboss-transaction-api_1.2_spec target/%{name}-core-impl.xml
%pom_change_dep org.jboss.spec.javax.transaction: :jboss-transaction-api_1.2_spec target/%{name}-jdbc.xml
%pom_change_dep org.jboss.spec.javax.transaction: :jboss-transaction-api_1.2_spec target/%{name}-spec-api.xml

%install
mkdir -p $RPM_BUILD_ROOT%{_javadir}/%{name} \
         $RPM_BUILD_ROOT%{_mavenpomdir} \
         $RPM_BUILD_ROOT%{_javadocdir}/%{name}
for m in common-api common-impl common-spi core-api core-impl deployers-common jdbc spec-api validator; do
  install -pm 644 target/%{name}-${m}.jar $RPM_BUILD_ROOT%{_javadir}/%{name}/%{name}-${m}.jar
  install -pm 644 target/%{name}-${m}.xml $RPM_BUILD_ROOT%{_mavenpomdir}/JPP.%{name}-%{name}-${m}.pom
  %add_maven_depmap JPP.%{name}-%{name}-${m}.pom %{name}/%{name}-${m}.jar
done
cp -rp target/docs/* $RPM_BUILD_ROOT%{_javadocdir}/%{name}

%files -f .mfiles
%doc README.md
%license LICENSE.txt

%files help
%{_javadocdir}/%{name}
%license LICENSE.txt

%changelog
* Wed Nov 09 2022 Ge Wang <wangge20@h-partners.com> - 1.3.4-2
- Bring source file into correspondence with descirbed in spec file

* Mon Aug 24 2020 huanghaitao <huanghaitao8@huawei.com> - 1.3.4-1
- package init
